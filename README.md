# CORS Tester
JavaScript React application and Express API that explains CORS.

![](./docs/app.png)

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Run Express API: `npm run express`
- Run React App: `npm run start`

# Question
Suppose www.xyz.com is the website for your bank.  When you type www.xyz.com into your browser you expect that the interaction between you and your bank starts with your finger tips and ends at www.xyz.com.  Suppose you log into www.xyz.com and your browser starts sending requests to www.badactor.com.  Would that be alarming or normal?  

# What is CORS?
CORS is a security feature in browsers that prevent requests from being sent to a domain that was not the original target domain specified by the user.  CORS stands for Cross Origin
Resource Sharing, where "origin" is our original target domain.  CORS uses client side preflight requests and custom headers (AKA CORS headers) to implement this security measure.  It's our responsibility to properly configure the CORS headers on the server side.

### Preflight Requests
The browser sends a preflight request to the server for requests that are not simple.  A preflight request is an HTTP OPTIONS request that determines if the actual request is safe to send.  A preflight request is sent before the intended HTTP request.

Preflight requests are not sent for [simple requests](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS#simple_requests).  Preflight requests are sent for non-simple requests.  Any request that contains an `Authorization` header is a non-simple request and triggers a preflight request.

![](./docs/cors1.png)

### CORS Headers
The API sends custom headers which tell the browser to relax the cross origin resource sharing security feature.  For example:

```
Access-Control-Allow-Origin: http://localhost:3000
Access-Control-Expose-Headers: x-api-version
```

![](./docs/cors2.png)

The API server sends an `Access-Control-Allow-Origin` header indicating that it is safe to communicate with me as long as your domain is: http://localhost:3000.  The server also sends a list of custom exposed headers using the `Access-Control-Expose-Headers` header key so that Axios can read the version of the API.

# The Browser is not Postman!
Postman does NOT issue a preflight OPTIONS request for simple requests. There is no concept of simple requests in Postman. Developers are often surprised when their API does not work with the web browser, but works fine with Postman.  Postman is a lightweight ReST client and the browser is much more sophisticated.

# POST vs PUT
The difference between PUT and POST is that PUT is idempotent: calling it once or several times successively has the same effect (that is no side effect), where successive identical POST calls may have additional effects, like passing an order several times.

# CORS Headers
### Request
|Header name           |Description                          |
|----------------------|-------------------------------------|
|Origin                |Combination of protocol, domain, and port of the browser tab opened|
|Access-Control-Request-Method|For the preflight request, specifies what method the original CORS request will use|
|Access-Control-Request-Headers|For the preflight request, a comma separated list specifying what headers the original CORS request will send|

### Response
|Header name           |Description                          |
|----------------------|-------------------------------------|
|*Access-Control-Allow-Origin|The allowed origins for this request as specified by the server. If it doesn’t match the Origin header and is not a *, browser will reject the request. If domain is specified, protocol component is required and can only be a single domain|
|Access-Control-Allow-Credentials|	CORS requests normally don’t include cookies to prevent CSRF attacks. When set to true, the request can be made with/will include credentials such as Cookies. The header should be omitted to imply false which means the CORS request will not be returned to the open tab. Cannot be used with wildcard|
|*Access-Control-Expose-Headers|	A whitelist of additional response headers to be exposed to the browser tab beyond the default headers|
|Access-Control-Max-Age|Value in seconds to cache preflight request results (i.e the data in Access-Control-Allow-Headers and Access-Control-Allow-Methods headers). Firefox maximum is 24 hrs and Chromium maximum is 10 minutes. Higher will have no effect.|
|Access-Control-Allow-Methods|Can be * to allow all methods. A comma separated whitelist of allowed methods that can be used for the CORS request.|
|Access-Control-Allow-Headers|Can be * to allow any header. A comma-separated whitelist of allowed headers that can be used for the CORS request.|

\* We configured these headers in this project.

